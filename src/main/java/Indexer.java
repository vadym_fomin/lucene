import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Vadim on 05.11.2017.
 */
public class Indexer {

    private IndexWriter writer;



    public Indexer(String indexDirectoryPath) throws IOException {
        //this directory will contain the indexes
        Directory indexDirectory =
                FSDirectory.open(new File(indexDirectoryPath));

        //create the indexer
        writer = new IndexWriter(indexDirectory, new IndexWriterConfig(Version.LUCENE_36, new StandardAnalyzer(Version.LUCENE_36)));
    }

    public void close() throws CorruptIndexException, IOException {
        writer.close();
    }

    private Document getDocument(File file) throws IOException {
        Document document = new Document();

        //index file contents
        Field contentField = new Field("contents", new FileReader(file));
        //index file name
        Field fileNameField = new Field("filename",
                file.getName(),Field.Store.YES,Field.Index.NOT_ANALYZED);
        //index file path
        Field filePathField = new Field("filepath",
                file.getCanonicalPath(),Field.Store.YES,Field.Index.NOT_ANALYZED);

        document.add(contentField);
        document.add(fileNameField);
        document.add(filePathField);

        return document;
    }

    private void indexFile(File file) throws IOException {
        System.out.println("Indexing "+file.getCanonicalPath());
        Document document = getDocument(file);
        writer.addDocument(document);
        close();
    }

    private void updateIndex(File file) throws IOException {
        writer.updateDocument(new Term
                ("contents",
                        file.getName()),new Document());
    }

    private List<String> search(String queryString) throws IOException, ParseException {
        Directory indexDirectory =
                FSDirectory.open(new File("D:\\index"));
        IndexReader indexReader= DirectoryReader.open(indexDirectory);
        final IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        QueryParser queryParser = new QueryParser(Version.LUCENE_42, "contents", new StandardAnalyzer(Version.LUCENE_42));
        Query query = queryParser.parse(queryString);
        TopDocs topDocs = indexSearcher.search(query, 10);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        return Arrays.asList(scoreDocs).stream().map(e-> {
            String document = null;
            try {
                document =  indexSearcher.doc(e.doc).get("filename");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return document;
        }
        ).collect(Collectors.toList());
    }


    public static void main(String[] args) throws IOException, ParseException {
        String indexDirectory = "D:\\index";
        Indexer indexer = new Indexer(indexDirectory);
        //indexer.indexFile(new File("D:\\folders.txt"));
        indexer.updateIndex(new File("D:\\folders.txt"));
        System.out.println(indexer.search("java"));
        indexer.close();
    }
}
